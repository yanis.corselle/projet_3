import os
import os.path
import csv
import sys
import sqlite3
import argparse
import logging

def creation_table():
    connexion = sqlite3.connect('BDD.sqlite3')
    cursor = connexion.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS vehicules(
    adresse_titulaire TEXT,
    nom TEXT,
    prenom TEXT,
    immatriculation TEXT PRIMARY KEY,
    date_immatriculation TEXT,
    vin Integer,
    marque Text,
    couleur TEXT,
    carrosserie TEXT,
    categorie TEXT,
    cylindree TEXT,
    energie Integer,
    places Integer,
    poids Integer,
    puissance Integer,
    type Text,
    variante Text,
    version Text,
    denomination_commerciale Text
)''')    
    logging.info('creation BDD')
    connexion.commit()


def VerificationExistenceVehicule(immatriculation):
    existe = False
    connection = sqlite3.connect('BDD.sqlite3')
    cursor=connection.cursor()
    cursor.execute("SELECT immatriculation FROM vehicules WHERE immatriculation= (immatriculation)")
    colonnes=cursor.fetchall()
    if len(colonnes) > 0: 
        existe=True
        logging.info('La table existe')
    connection.close()
    return existe
 
def InsertionBDD(immatriculation):
    connection = sqlite3.connect('BDD.sqlite3')
    cursor=connection.cursor()
    cursor.execute("INSERT INTO vehicules (immatriculation) VALUES(?)")
    connection.commit()
    logging.info('Insertion faite')
    connection.close()

def MajBDD(valeur, immatriculation):
    connection = sqlite3.connect('BDD.sqlite3')
    cursor=connection.cursor()
    cursor.execute("UPDATE vehicules VALUES (?) WHERE immatriculation = (immatriculation)")
    connection.commit()
    logging.info('MAJ faite')
    connection.close()
    
def read_csv_file(path):
    list_data = []
    names = []
    logging.info("Construction du dictionnaire")
    with open(path) as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        i = 0
        row = dict()
        for row in reader:
            if len(names) == 0:
                logging.info("Extraction des noms des colonnes")
                for name in row:
                    names.append(name)
            else:
                list_data.append({})
                for indice in range(len(row)):
                    list_data[i][names[indice]] = row[indice]
                i+=1
    return list_data




if __name__ == "__main__":
    parser=argparse.ArgumentParser(description='ImportBDD')
    parser.add_argument('file', help = 'input source file path')
    args=parser.parse_args()
    
    fichier=open(args.file,'r')
    lignes = read_csv_file(args.file)
    fichier.close()
    longueur_lignes = len(lignes)
    immatriculations = []    
    creation_table()
    for i in range(longueur_lignes):
        for key in lignes[i].items():
            vals=lignes[i].values()
            immatriculation=lignes[i].get("immatriculation")
            test=VerificationExistenceVehicule(immatriculation)
            if test==False:
                InsertionBDD(immatriculation)
            else:
                MajBDD(vals, immatriculations)
    