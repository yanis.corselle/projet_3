import unittest
import ImportBDD

class TestImportBDD(unittest.Testcase):

    def test_VerificationExistenceBDD(VerificationExistenceBDD):
        self.assertEqual(ImportBDD.VerificationExistenceBDD("base de donnee"),false)
    
    def test_VerificationExistenceTable(VerificationExistenceTable):
        self.assertEqual(ImportBDD.VerificationExistenceTable("table"),false)
    
    def test_VerificationExistenceVehicule(VerificationExistenceVehicule):
        self.assertEqual(ImportBDD.VerificationExistenceVehicule("FA-235-FB"),false)

    def test_CreationBDD(CreationBDD):
        ImportBDD.CreationBDD("BDDtest")
        self.assertEqual(ImportBDD.VerificationExistenceBDD("BDDtest"),true)

    def test_CreationTable(CreationTable):
        ImportBDD.CreationTable(BDD.csv,dictionaire)
        self.assertEqual(ImportBDD.VerificationExistencetable("immatriculation"),true)

    def test_InsertionBDD(InsertionBDD):
        ImportBDD.InsertionBDD(BDD.csv)
        self.assertEqual(ImportBDD.VerificationExistenceVehicule("FA-235-FB"),true)
    
    def test_MajBDD(MajBDD):
        ImportBDD.MajBDD(BDD.csv)
        self.assertEqual(ImportBDD.VerificationExistenceVehicule("FA-235-FB"),false)
        self.assertEqual(ImportBDD.VerificationExistenceVehicule("FA-240-FB"),true)

    def test_ProductionFichierLog(ProductionFichierLog):
        self.assertEqual(ImportBDD.ProductionFichierLog("fichier"),fichier.log)